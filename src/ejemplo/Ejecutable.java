package ejemplo;

import java.util.Enumeration;
import java.util.Hashtable;

public class Ejecutable {
    public static  void main(String args[]){
        System.out.println("Ejercicio de HashTable en Java 18.");
        Hashtable<String, String> tabla = new Hashtable<String, String>();
        tabla.put("IronMan", "Marvel");
        tabla.put("Bartman", "Disney");
        tabla.put("Batman", "DC");
        tabla.put("Superman", "DC");
        System.out.println(tabla.get("Batman"));
        //
        Persona persona1 = new Persona("Jose", 35);
        Persona persona2 = new Persona("Carlos", 45);
        Persona persona3 = new Persona("María", 35);
        Persona persona4 = new Persona("Alejandra", 25);
        //
        Hashtable <String, Persona> tabla2 = new Hashtable<String, Persona>();
        tabla2.put(persona1.getName(), persona1);
        tabla2.put(persona2.getName(), persona2);
        tabla2.put(persona3.getName(), persona3);
        tabla2.put(persona4.getName(), persona4);
        //Imprimir
        Enumeration<String> keys = tabla2.keys();
        while (keys.hasMoreElements()){
            String nombre = keys.nextElement();
            System.out.println(tabla2.get(nombre));
        }
    }
}
